package com.example.jscaria.weathertoday;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.StrictMode;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.JsonReader;
import android.view.View;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import org.json.JSONObject;
import org.w3c.dom.Text;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;



public class MainActivity extends AppCompatActivity {
// sup man!


    TextView txtlat; //= (TextView)findViewById(R.id.degreeText);
    TextView txtlong; //= (TextView) findViewById(R.id.countyText);


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        txtlat = (TextView) findViewById(R.id.degreeText);

        txtlong = (TextView) findViewById(R.id.countyText);

        LocationManager lm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        LocationListener ll = new myLocationListener();

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.


            return;
        }
        lm.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, ll);

    }


        class myLocationListener implements LocationListener
        {

            @Override
            public void onLocationChanged(Location location) {

                if(location != null)
                {
                    double pLong = location.getLongitude();
                    double pLat = location.getLatitude();


                    txtlat.setText(Double.toString(pLat));
                    txtlong.setText(Double.toString(pLong));


                }

            }

            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {

            }

            @Override
            public void onProviderEnabled(String provider) {

            }

            @Override
            public void onProviderDisabled(String provider) {

            }
        }



    public void button_onClick(View view) {

        EditText text = (EditText) findViewById(R.id.editText);


        TextView degreeT = (TextView)findViewById(R.id.degreeText);
        TextView countyT = (TextView) findViewById(R.id.countyText);


        TextView test = (TextView)findViewById(R.id.textView);


        if(!isInteger(text.getText().toString()) || text.getText().toString().length() != 5)
        {

            countyT.setText("");
            degreeT.setText("Invalid zip code.");
            return;
        }


        InputStream response = null;
        String result,county,temperature = null;
        String apiCall = "http://api.openweathermap.org/data/2.5/weather?zip="+text.getText().toString()+"&units=Imperial&appid=9503782276ab0065fa67b0b0c7063fc6";
        try {
            result = (HttpConn.downloadContent(apiCall));
            JSONObject obj = new JSONObject(result);
            county = obj.getString("name") + "";
            countyT.setText(county);
            temperature = obj.getJSONObject("main").getString("temp");
            degreeT.setText(temperature + "" + (char) 0x00B0 + "F");
        }
        catch(IOException ex)
        {

        }
        catch (org.json.JSONException ex)
        {

        }


        //JsonReader reader = Json.createReader();

    }


    public static boolean isInteger(String s) {
        try {
            Integer.parseInt(s);
        } catch(NumberFormatException e) {
            return false;
        } catch(NullPointerException e) {
            return false;
        }
        // only got here if we didn't return false
        return true;
    }
}
